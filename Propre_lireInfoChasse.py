import numpy as np
import pyautogui
import cv2
import time
import sys
import pytesseract 

import colorama
from colorama import Fore, Back, Style
from python_imagesearch.imagesearch import imagesearch, imagesearch_loop, imagesearch_count
from Propre_seleniumDofusChasse import *
from requeteDofusChasse import *
from clique import *

prevNbFlecheHaut,prevNbFlecheBas,prevNbFlecheGauche,prevNbFlecheDroite = 0,0,0,0

def chercherPhorreur():
	# input("Ok pour chercherPhorreur")
	pos = imagesearch("./pictures_34pouces/Phorreur1.png")
	if pos != [-1, -1]:
		# print("Phorreur 1 Trouvé")
		return True
	pos = imagesearch("./pictures_34pouces/Phorreur1.png")
	if pos != [-1, -1] :
		# print("Phorreur 2 Trouvé")
		return True
	pos = imagesearch("./pictures_34pouces/Phorreur1.png")
	if pos != [-1, -1] :
		# print("Phorreur 3 Trouvé")
		return True
	pos = imagesearch("./pictures_34pouces/Phorreur1.png")
	if pos != [-1, -1]:
		print("Phorreur 4 Trouvé")
		return True
	# print("Pas de Phorreur Trouvé")
	return False

def test():
	colorama.init()
	# pos = imagesearch_loop("./pictures/menu/phorreur1.png",1)

	chercherPhorreur()

	width, height= pyautogui.size()
	if (width==3440) & (height==1440):
		pos = imagesearch_loop("./pictures_34pouces/ChasseEnCours.png",1)
	else:
		pos = imagesearch_loop("./pictures/menu/ChasseEnCours.png",1)
	print(f"X = {pos[0]} Y = {pos[1]}")

	if (width==3440) & (height==1440):
		posbas = imagesearch_loop("./pictures_34pouces/basFenetreChasse.png",1)
	else:
		posbas = imagesearch_loop("./pictures/menu/basFenetreChasse.png",1)
	print(f"X = {posbas[0]} Y = {posbas[1]}")

	pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"


	# img = pyautogui.screenshot()
	# img = pyautogui.screenshot(region=(1300,130, 230, 140))
	if (width==3440) & (height==1440):
		img = pyautogui.screenshot(region=(pos[0]+60,pos[1]+95, 330, posbas[1]-pos[1]-100))
	else :
		img = pyautogui.screenshot(region=(pos[0]+50,pos[1]+70, 230, posbas[1]-pos[1]-70))
	# while (True) :
		# print(pyautogui.position())

	#img = pyautogui.screenshot()
	open_cv_image = np.array(img)
	#Avoir l'image en rgb
	open_cv_image = open_cv_image[:, :, ::-1].copy()
	img_gray = cv2.cvtColor(open_cv_image, cv2.COLOR_BGR2GRAY)
	# cv2.imshow('image',open_cv_image)
	cv2.imshow('image',img_gray)


	# text = pytesseract.image_to_string(open_cv_image)
	text = pytesseract.image_to_string(img_gray)
	# print(Fore.BLUE + text + Style.RESET_ALL)
	# print(text)
	splittedText = text.split("\n")
	indice = []
	for ligne in splittedText :
		if len(ligne) >  3:
			indice.append(ligne)
			print(ligne)
		
	k = cv2.waitKey(0)

def LireInfoFenetreIndice():
	#Chemin vers l'outil de reconnaissance de texte
	pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"	
	
	#Cherche la position de la fenetre de chasse, pour pouvoir réaliser la reconnaissance de texte sur seulement cette partie de l'écran
	haut_fenetre = imagesearch_loop("./pictures_34pouces/ChasseEnCours.png",1)
	bas_fenetre = imagesearch_loop("./pictures_34pouces/basFenetreChasse.png",1)

	#Réalise un crop sur la fenetre de chasse
	img_fenetre_indice = pyautogui.screenshot(region=(haut_fenetre[0]+60,haut_fenetre[1]+95, 330, bas_fenetre[1]-haut_fenetre[1]-100))
	
	#Avoir l'image sous forme de matrice
	open_cv_image = np.array(img_fenetre_indice)
	#Convertir l'image en teinte de gris
	open_cv_image = open_cv_image[:, :, ::-1].copy()
	img_gray = cv2.cvtColor(open_cv_image, cv2.COLOR_BGR2GRAY)

	#Réaliser la reconnaissance de texte sur l'image grise
	text = pytesseract.image_to_string(img_gray)

	#Supprimer les éléments vides sans information
	splittedText = text.split("\n")
	liste_indices = []
	for ligne in splittedText :
		#si le mot fait plus de 3 caractère on considère que ce n'est pas du bruit et on l'ajoute à la liste d'indices
		if len(ligne) >  3:
			liste_indices.append(ligne)
			print(ligne)		
	return liste_indices
	
	
	
def ChercherPointDeDépart(liste_indices):
	#Le premier element de la liste des indices est toujours les coordonnées de départ de la chasse
	coordonnéesDepart_x = liste_indices[0].split(",")[0].split("[")[1]
	coordonnéesDepart_y = liste_indices[0].split(",")[1].split("]")[0]
	#print(coordonnéesDepart_x)
	#print(coordonnéesDepart_y)
	return [coordonnéesDepart_x,coordonnéesDepart_y]

def ChercherDirectionDernierIndice():
    global prevNbFlecheHaut, prevNbFlecheBas, prevNbFlecheGauche, prevNbFlecheDroite
    print(f"Nombre de Fleches Haut {imagesearch_count('./pictures_34pouces/flecheHaut.png')}")
    print(f"Nombre de Fleches Bas {imagesearch_count('./pictures_34pouces/flecheBas.png')}")
    print(f"Nombre de Fleches Gauche {imagesearch_count('./pictures_34pouces/flecheGauche.png')}")
    print(f"Nombre de Fleches Droite {imagesearch_count('./pictures_34pouces/flecheDroite.png')}")
    
    if imagesearch_count('./pictures_34pouces/flecheHaut.png') - prevNbFlecheHaut > 0 :
        prevNbFlecheHaut += 1
        return "top"	
    if imagesearch_count('./pictures_34pouces/flecheBas.png') - prevNbFlecheBas > 0 :
        prevNbFlecheBas += 1
        return "bottom"
    if imagesearch_count('./pictures_34pouces/flecheGauche.png') - prevNbFlecheGauche > 0 :
        prevNbFlecheGauche += 1
        return "left"
    if imagesearch_count('./pictures_34pouces/flecheDroite.png') - prevNbFlecheDroite > 0 :
        prevNbFlecheDroite += 1
        return "right"
		
	
#obsolete requetage plus efficace
def ChercherPosIndiceSelenium(liste_indices,coordonnéesDepart):
	dernier_indice = liste_indices[-1]
	Direction = ChercherDirectionDernierIndice()
	print(Direction)
	x_to_go, y_to_go = searchPosition(coordonnéesDepart[0],coordonnéesDepart[1],Direction,dernier_indice)
	print(x_to_go,y_to_go)
	return x_to_go, y_to_go

def ChercherPosIndiceRequete(liste_indices,coordonnéesDepart):
	dernier_indice = liste_indices[-1]
	Direction = ChercherDirectionDernierIndice()
	print(Direction)
	x_to_go, y_to_go = searchPositionRequete(coordonnéesDepart[0],coordonnéesDepart[1],Direction,dernier_indice)
	print(x_to_go,y_to_go)
	return x_to_go, y_to_go
	
def getCurrentPose():
	pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"	
	
	#Réalise un crop sur la fenetre de chasse
	# img_fenetre_indice = pyautogui.screenshot(region=(413,
													  # 85,
													  # 235,
													  # 47))
	img_fenetre_indice = pyautogui.screenshot(region=(400,
													  45,
													  550,
													  85))
	
	#Avoir l'image sous forme de matrice
	open_cv_image = np.array(img_fenetre_indice)
	#Convertir l'image en teinte de gris
	open_cv_image = open_cv_image[:, :, ::-1].copy()
	img_gray = cv2.cvtColor(open_cv_image, cv2.COLOR_BGR2GRAY)
	# (thresh, blackAndWhiteImage) = cv2.threshold(img_gray, 127, 255, cv2.THRESH_BINARY)
	(thresh, blackAndWhiteImage) = cv2.threshold(img_gray, 220, 255, cv2.THRESH_BINARY)
	# (thresh, blackAndWhiteImage) = cv2.threshold(img_gray, 127, 255, cv2.THRESH_BINARY)
	blackAndWhiteImage = cv2.bitwise_not(blackAndWhiteImage)
	# cv2.imshow('Black white image', blackAndWhiteImage)
	# cv2.imshow('image',img_gray)
	
	#Réaliser la reconnaissance de texte sur l'image grise
	# text = pytesseract.image_to_string(img_gray)
	
	# text = pytesseract.image_to_string(blackAndWhiteImage, config='digits')
	text = pytesseract.image_to_string(blackAndWhiteImage)
	# print(text)
	
	# text = pytesseract.image_to_string(img_gray)
	# print(text)
	
	# k = cv2.waitKey(0)
	
	# clean_text = text.replace(" ","").split(",")
	# clean_text = text.split("\n")[1].replace(" ","").split(",")
	for ligne in text.split("\n"):
		if "Niveau" in ligne:
			splited_ligne = ligne.split(",")
			current_x, current_y = splited_ligne[0],splited_ligne[1]
			break
	# print(clean_text)
	# current_x, current_y = clean_text[0],clean_text[1]
	# print(current_x, current_y )
	# vérification que current_x et current_y castable en int
	return int(current_x),int(current_y)
	
def commencerChasse():
	liste_indices = LireInfoFenetreIndice()
	coordonnéesDepart = ChercherPointDeDépart(liste_indices)
	goTo(coordonnéesDepart[0],coordonnéesDepart[1])
	return liste_indices, coordonnéesDepart
	
def main():
	# print("ok")
	# getCurrentPose()
	liste_indices,coordonnéesDepart = commencerChasse()
	# goTo(-28,-34)
	# x_to_go, y_to_go = ChercherPosIndiceSelenium(liste_indices,coordonnéesDepart)
	x_to_go, y_to_go = ChercherPosIndiceRequete(liste_indices,coordonnéesDepart)
	goTo(x_to_go,y_to_go)
	
if __name__  == "__main__" :
	main()	
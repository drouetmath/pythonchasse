from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException 
from selenium.webdriver.firefox.options import Options
import time


def check_exists_by_xpath(driver, xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True
	
# def searchPosition(x,y,direction,indice,driver):
def searchPosition(x,y,direction,indice):
	options = Options()
	# Pas le meme résultat sans header
	options.headless = True
	driver = webdriver.Firefox(options=options)
	#driver = webdriver.Firefox()
	
	#Chrome ne marche pas sans header
	#options = webdriver.ChromeOptions();
	#options.add_argument('headless');
	#options.add_argument('window-size=1200x600')-; # optional
	#driver = webdriver.Chrome(chrome_options=options)
	driver.get("https://dofus-map.com/hunt")
	time.sleep(0.5)
	
	x_input = driver.find_element(By.XPATH, '//*[@id="x"]')
	x_input.send_keys(x)
	x_input.send_keys(Keys.RETURN)
	time.sleep(0.5)
	
	y_input = driver.find_element(By.XPATH, '//*[@id="y"]')
	y_input.send_keys(y)
	y_input.send_keys(Keys.RETURN)
	time.sleep(0.5)
	
	direction_arrow = driver.find_element(By.XPATH, '//*[@id="'+direction+'"]')
	ActionChains(driver).click(direction_arrow).perform()
	
	#Si demande où etes vous
	time.sleep(0.5)
	if check_exists_by_xpath(driver,'/html/body/div[2]/div/div/span[1]'):
		Amakna = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/span[1]')
		ActionChains(driver).click(Amakna).perform()
	
	
	time.sleep(0.5)
	indice_input = driver.find_element(By.XPATH,'//*[@id="hintName"]')
	indice_input.send_keys(indice)
	indice_input.send_keys(Keys.RETURN)
	
	time.sleep(0.5)
	map_to_move  = driver.find_element(By.XPATH, '//*[@id="firstLine"]').text

	pos_to_go  = driver.find_element(By.XPATH, '//*[@id="secondLine"]').text
	pos_to_go = pos_to_go.replace('[', '').replace(']', '').replace(' ','').split(";")
	x_to_go, y_to_go = pos_to_go[0],pos_to_go[1]
	time.sleep(0.5)
	

	driver.close()
	#return int(x_to_go), int(y_to_go) , int(map_to_move) 
	return int(x_to_go), int(y_to_go)  
	
# x int, y int, direction string,indice string, driver webdriver
# inputPosition(x,y,direction,indice,driver)

def main():
	# x_to_go, y_to_go , map_to_move  = searchPosition(-15,-52,"right","Rondin de bois",driver)
	# x_to_go, y_to_go , map_to_move  = searchPosition(-15,-52,"right","Rondin de bois")
	# x_to_go, y_to_go , map_to_move  = searchPosition(-30,35,"left","Rouleau de métal posé")
	# ok pour le a à la place de à
	x_to_go, y_to_go , map_to_move  = searchPosition(-24,-38,"left","Charrette a 4 roues")
	print(x_to_go)
	print(y_to_go)
	print(map_to_move)

if __name__  == "__main__" :
	main()
	
	
	
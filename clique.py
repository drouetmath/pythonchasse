from pywinauto.application import Application
from python_imagesearch.imagesearch import imagesearch, imagesearch_loop
from Propre_lireInfoChasse import getCurrentPose
import colorama
from colorama import Fore, Back, Style

from PIL import Image

import matplotlib.pyplot as plt
import scipy 
from scipy.spatial import distance
from random import random,randint,choice,uniform
import numpy as np

import pyautogui
import pytweening
import os
import time
import sys
import math

def random_curve():
	# curvelist = [pytweening.easeInCubic,pytweening.easeOutCubic,pytweening.easeInOutCubic]
	curvelist = [pytweening.easeInCubic,pytweening.easeOutCubic,pytweening.easeInCubic,pytweening.easeOutCubic,pytweening.easeInOutCubic]
	return choice(curvelist)
	
#Max_speed => 1.25 secondes pour parcourir la distance maximale sur l'écran	
def moveTo_curve(pos_x_to_go, pos_y_to_go, max_speed =  1.25, max_offset = 300):
	pyautogui.getPointOnLine = getPointOnCurve
	choosed_curve = random_curve()
	# time.sleep(0.5)
	pos_x_mouse,pos_y_mouse = pyautogui.position()		
	size_x_screen, size_y_screen = pyautogui.size()	
	
	dist = distance.euclidean((pos_x_mouse,pos_y_mouse),(pos_x_to_go,pos_y_to_go)) 
	dist_max = distance.euclidean((0,0),(size_x_screen,size_y_screen)) 
	# ratio_dist = (dist/dist_max)
	ratio_dist = (dist/dist_max)*uniform(0.75,1.25)
	# print(f"ratio_dist {ratio_dist}")
	# time_mv = max_speed*ratio_dist
	
	time_mv = math.log(1.5 + max_speed*ratio_dist)*uniform(0.75,1.25)
	# print(f"time_mv {time_mv}")
	
	max_offset_dist = int(ratio_dist*max_offset) 
	# rand_offset = randint(0,max_offset_dist)
	if max_offset_dist > 50 :
		rand_offset = int(randint(50,max_offset_dist)*uniform(0.75,1.25))
	else :
		rand_offset = int(randint(max_offset_dist,50)*uniform(0.75,1.25))
		
	# print(f"rand_offset = {rand_offset}")
	set_curve(getPointOnCurve, choosed_curve, rand_offset)
	
	pyautogui.dragTo(pos_x_to_go, pos_y_to_go, time_mv, button='left') 
	# pyautogui.easeOutQuad (déplaclement rapide au début et lent à la fin)
	# pyautogui.moveTo(pos_x_to_go,pos_y_to_go,time_mv,pyautogui.easeOutQuad)
	
	
	
def getPointOnCurve(x1, y1, x2, y2, n, tween=None, offset=0):
    """Returns the (x, y) tuple of the point that has progressed a proportion
    n along the curve defined by the two x, y coordinates.
    If the movement length for X is great than Y, then Y offset else X
    """
    # for compatibility Backward
    if getPointOnCurve.tween and getPointOnCurve.offset:  # need DEL
        tween = getPointOnCurve.tween                     # need DEL
        offset = getPointOnCurve.offset                   # need DEL

    x = ((x2 - x1) * n) + x1
    y = ((y2 - y1) * n) + y1
    if tween and offset:
        offset = (n - tween(n)) * offset
        if abs(x2 - x1) > abs(y2 - y1):
            y += offset
        else:
            x += offset
    return (x, y)

# getPointOnCurve.tween = None
# getPointOnCurve.offset = 0

def set_curve(func, tween=None, offset=0):
   func.tween = tween
   func.offset = offset


def imagesearch_loop_click(pic,time_check = 1,max_speed = 1.25):
	
	im = Image.open(pic)
	w, h = im.size
	# print(Fore.BLUE + 'TAILLE IMAGE ' + f"width = {w} height = {h}")
	# print(f"width = {w} height = {h}")
	# print(Style.RESET_ALL)
	# mu, sigma = 0.7, 0.1
	# mu, sigma = 4, 0.1
	# second_to_go_position_finded = np.random.normal(mu, sigma, 1)
	# print(f"second_to_go_position_finded = {second_to_go_position_finded}")
	pos = imagesearch_loop(pic, time_check)
	# pyautogui.moveTo(pos[0],pos[1],duration=second_to_go_position_finded)
	# moveTo_curve(pos[0],pos[1],max_speed,pyautogui.easeOutQuad)

	random_position_x_on_pic = randint(pos[0],pos[0]+w)
	random_position_y_on_pic = randint(pos[1],pos[1]+h)
	moveTo_curve(random_position_x_on_pic,random_position_y_on_pic,max_speed)
	
	# pyautogui.moveTo(pos[0],pos[1],second_to_go_position_finded,pyautogui.easeOutQuad)
	
	wait_time_for_click = np.random.normal(0.5, 0.05, 1)
	# print(f"wait_time_for_click = {wait_time_for_click}")
	time.sleep(wait_time_for_click)
	
	pyautogui.click()
	

def ChangerMapGauche(max_speed = 1.25):
	random_position_x_on_pic = randint(866,889)
	random_position_y_on_pic = randint(38,1188)
	moveTo_curve(random_position_x_on_pic,random_position_y_on_pic,max_speed)
	pyautogui.click()
	time.sleep(5*uniform(0.85,1.15))
	
def ChangerMapDroite(max_speed = 1.25):
	random_position_x_on_pic = randint(2540,2563)
	random_position_y_on_pic = randint(47,1087)
	moveTo_curve(random_position_x_on_pic,random_position_y_on_pic,max_speed)
	pyautogui.click()
	time.sleep(5*uniform(0.85,1.15))
	
def ChangerMapHaut(max_speed = 1.25):
	random_position_x_on_pic = randint(916,2500)
	random_position_y_on_pic = randint(38,44)
	moveTo_curve(random_position_x_on_pic,random_position_y_on_pic,max_speed)
	pyautogui.click()
	time.sleep(5*uniform(0.85,1.15))

def ChangerMapBas(max_speed = 1.25):
	random_position_x_on_pic = randint(947,2339)
	random_position_y_on_pic = randint(1220,1235)
	moveTo_curve(random_position_x_on_pic,random_position_y_on_pic,max_speed)
	pyautogui.click()
	time.sleep(5*uniform(0.85,1.15))

def goTo(x_to,y_to):
	x_from, y_from = getCurrentPose()
	x_to = int(x_to)
	y_to = int(y_to)
	# print((x_from != x_to) | (y_from != y_to))
	
	while (x_from != x_to) | (y_from != y_to):
		print(f"x_from = {x_from} y_from = {y_from}")
		print(f"x_to = {x_to} y_to = {y_to}")
	
		x_ok = x_from == x_to
		y_ok = y_from == y_to
		if (x_ok) and not(y_ok) :
			if y_from < y_to :
				ChangerMapBas()
			else:
				ChangerMapHaut()
		elif not(x_ok) and (y_ok) :
			if x_from < x_to :	
				ChangerMapDroite()
			else:
				ChangerMapGauche()
		else :
			if randint(0, 1) :
				if y_from < y_to :
					ChangerMapBas()
				else:
					ChangerMapHaut()
			else :
				if x_from < x_to :	
					ChangerMapDroite()
				else:
					ChangerMapGauche()
		time.sleep(2)			
		x_from, y_from = getCurrentPose()
		

	
	
	
	
def paint_test():
	pyautogui.getPointOnLine = getPointOnCurve
	
	print(Fore.RED + 'Demarage Paint')
	print(Style.RESET_ALL)
	pyautogui.click()
	# app = Application().start(r'C:\Windows\System32\mspaint.exe')
	for i in range (0,10):
	
		choosed_curve = random_curve()
		print(f"choosed_curve {choosed_curve}")
		# rand_offset = randint(0,300)
		# rand_offset = randint(50,200)
		# pyautogui.moveTo(200, 200)
		pyautogui.moveTo(randint(250,1000),randint(250,1000))
		time.sleep(0.5)
		
		pos_x_mouse,pos_y_mouse = pyautogui.position()
		pos_x_to_go,pos_y_to_go = randint(250,1000),randint(250,1000)
		# pos_x_to_go,pos_y_to_go = 1000,500
		# pos_x_to_go,pos_y_to_go = 255,255
		
		size_x_screen, size_y_screen = pyautogui.size()		
		dist = distance.euclidean((pos_x_mouse,pos_y_mouse),(pos_x_to_go,pos_y_to_go)) 
		print(f"distance {dist}")
		
		#1.5s pour parcourir la distance max (l'écran)
		# max_speed = 1.5 
		# max_speed = 2 
		# max_speed = 1.5 
		max_speed = 1.25 
		dist_max = distance.euclidean((0,0),(size_x_screen,size_y_screen)) 
		
		# ratio_dist = (dist/dist_max)
		ratio_dist = (dist/dist_max)*uniform(0.75,1.25)

		print(f"ratio_dist {ratio_dist}")
		# time_mv = 0.5
		# time_mv = max_speed*ratio_dist
		time_mv = math.log(1.5 + max_speed*ratio_dist)*uniform(0.75,1.25)
		print(f"time_mv {time_mv}")
		
		
		max_offset = 300
		max_offset_dist = int(ratio_dist*max_offset) 
		# rand_offset = randint(0,max_offset_dist)
		if max_offset_dist > 50 :
			rand_offset = int(randint(50,max_offset_dist)*uniform(0.75,1.25))
		else :
			rand_offset = int(randint(max_offset_dist,50)*uniform(0.75,1.25))
			
		print(f"rand_offset = {rand_offset}")
		set_curve(getPointOnCurve, choosed_curve, rand_offset)
	
		pyautogui.dragTo(pos_x_to_go, pos_y_to_go, time_mv, button='left') 
		

		
		# time_mv = 1 
		# time_mv = 0.4 
		# time_mv = np.random.normal(1, 0.1, 1) 
		# time_mv = np.random.normal(1, 0.1, 1) 
		
		
	
	# set_curve(getPointOnCurve, pytweening.easeOutCubic, 100)
	# pyautogui.moveTo(200, 200)
	# time.sleep(2)
	# pyautogui.dragTo(1000, 800, 3, button='left') 
	
	# set_curve(getPointOnCurve, pytweening.easeInCubic, 100)
	# pyautogui.moveTo(200, 200)
	# time.sleep(2)
	# pyautogui.dragTo(1000, 800, 3, button='left') 
	

	# set_curve(getPointOnCurve, pytweening.easeInOutCubic, 50)
	# pyautogui.moveTo(200, 200)
	# time.sleep(2)
	# pyautogui.dragTo(1000, 800, 3, button='left') 
	
	
	# set_curve(getPointOnCurve, pytweening.easeInOutCubic, 100)
	# pyautogui.moveTo(200, 200)
	# time.sleep(2)
	# pyautogui.dragTo(1000, 800, 3, button='left') 
	

	# set_curve(getPointOnCurve, pytweening.easeInOutCubic, 200)
	# pyautogui.moveTo(200, 200)
	# time.sleep(2)
	# pyautogui.dragTo(1000, 800, 3, button='left') 
	
	
def main():
	colorama.init()
	# random_curve()
	# app = Application().start(r'"C:\Users\Mathis\AppData\Local\Programs\zaap\Ankama Launcher.exe" --gameUid=dofus --release=main"')
	# imagesearch_loop_click("./pictures/menu/jouerLauncher.png", 1)
	# paint_test()
	# imagesearch_loop_click("./pictures/menu/JouerPersonnage.png")
	for i in range (0,5):
		# moveTo_curve(randint(500,1000),randint(500,700))
		imagesearch_loop_click("./pictures/menu/JouerPersonnage.png")

if __name__  == "__main__" :
	goTo(-26,-37)
	# main()

	
# Radius 
# R = 400
# measuring screen size
# (x,y) = pyautogui.size()
# locating center of the screen 
# (X,Y) = pyautogui.position(x/2,y/2)
# offsetting by radius 
# pyautogui.moveTo(X+R,Y)

# for i in range(360):
    # setting pace with a modulus 
    # if i%6==0:
       # pyautogui.moveTo(X+R*math.cos(math.radians(i)),Y+R*math.sin(math.radians(i)),0.2)
	   
	   
	   
# pyautogui.moveTo(1,1,0.5,pyautogui.easeOutQuad)


# set_curve(getPointOnCurve, pytweening.easeInCubic, 100)
# 


# pyautogui.getPointOnLine = getPointOnCurve  #   Replacement

# print('Curve easeInCubic')
# set_curve(getPointOnCurve, pytweening.easeInCubic, 100)
# print('>')
# pyautogui.moveTo(750,750,0.5,pyautogui.easeOutQuad)